﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DelegatedCalculator
{
    class Program
    {

        static void Main(string[] args)
        {
            DelegatedCalculator calc = new DelegatedCalculator();
            calc.Run();
        }
    }
}
