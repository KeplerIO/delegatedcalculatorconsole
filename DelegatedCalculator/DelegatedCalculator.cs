﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DelegatedCalculator
{
    class DelegatedCalculator
    {
        private int operand1;
        private int operand2;
        char operation;
        private int answer;

        char[] validOperations = { '+', '-', '*', '/' };


        public void Run()
        {
            this.GetInput();
            this.CalculateAndReturnAnswer();
        }

        private void GetInput()
        {
            Console.Write("Enter 1st operand: ");
            this.operand1 = Int32.Parse(Console.ReadLine());

            Console.Write("\nEnter 2nd operand: ");
            this.operand2 = Int32.Parse(Console.ReadLine()); Console.WriteLine();

            this.GetOperation();
        }

        private void GetOperation()
        {
            Console.Write("Enter the operation: ");

            char tmpOp = Console.ReadKey().KeyChar;
            if (!validOperations.Contains(tmpOp))
            {
                Console.WriteLine("\nPlease enter a valid operation (+, -, *, /)");
                this.GetOperation();
            }

            this.operation = tmpOp;
        }

        private void CalculateAndReturnAnswer()
        {
            switch (this.operation)
            {
                case '+':
                    this.answer = this.operand1 + this.operand2;
                    break;
                case '-':
                    this.answer = this.operand1 - this.operand2;
                    break;
                case '*':
                    this.answer = this.operand1 * this.operand2;
                    break;
                case '/':
                    this.answer = this.operand1 / this.operand2;
                    break;
            }

            Console.WriteLine($"Answer: {this.answer}");
            Console.ReadLine();
        }

    }
}
